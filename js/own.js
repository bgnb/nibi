jQuery(function($) {
/*
$.keyboard.keyaction.enter = function(base){
  console.log('enter');
};
*/
$('#own').keyboard({

		display: {
			'bksp'    : '\u2190',
			'shift'   : '\u21d1',
			'meta1'   : '.?123',
			'accept'  : '\u21d3',
			'meta2'   : 'abc',
			'default' : '\u0430\u0431\u0432',
			'clear'   : 'clear',
		},

		layout: 'custom',
		//restrictInput : true, // Prevent keys not in the displayed keyboard from being typed in
		//maxLength : 6,
		//lockInput: true, // prevent manual keyboard entry
		useCombos : false, // don't want A+E to become a ligature
		preventPaste : true,  // prevent ctrl-v and right click
		autoAccept : true,
		//usePreview: false, // no preveiw
		
		customLayout: {

			'default' : [
					"\u0439 \u0446 \u0443 \u043a \u0435 \u043d \u0433 \u0448 \u0449 \u0437 \u0445 \u044a {bksp}",
					"\u0444 \u044b \u0432 \u0430 \u043f \u0440 \u043e \u043b \u0434 \u0436 \u044d {enter}",
					"{shift} \u044f \u0447 \u0441 \u043c \u0438 \u0442 \u044c \u0431 \u044e {shift} {clear}",
					"{meta1} {meta2} {space} {meta1} {accept}"
				],

			'shift' : [
					"\u0419 \u0426 \u0423 \u041a \u0415 \u041d \u0413 \u0428 \u0429 \u0417 \u0425 \u042a {bksp}",
					"\u0424 \u042b \u0412 \u0410 \u041f \u0420 \u041e \u041b \u0414 \u0416 \u042d {enter}",
					"{shift} \u042f \u0427 \u0421 \u041c \u0418 \u0422 \u042c \u0411 \u042e {shift} {clear}",
					"{meta1} {meta2} {space} {meta1} {accept}"
				],
				
			'meta2': [
					'q w e r t y u i o p {bksp}',
					'a s d f g h j k l {enter}',
					'{shift} z x c v b n m {shift} {clear}',
					'{meta1} {default} {space} {meta1} {accept}'
				],
				
			'meta2-shift': [
					'Q W E R T Y U I O P {bksp}',
					'A S D F G H J K L {enter}',
					'{shift} Z X C V B N M {shift} {clear}',
					'{meta1} {default} {space} {meta1} {accept}'
				],
				
			'meta1': [
					'1 2 3 4 5 6 7 8 9 0 {bksp}',
					'- \u2116 : ; ( ) % = _ {enter}',
					'. , + ! " {sp:1} {clear}',
					'{default} {space} {meta2} {accept}'
				],
		},
		
		/*css : {
			buttonAction : 'ui-state-active'
		},*/

	});
	
$('#i-search').keyboard({

		display: {
			'bksp'    : '\u2190',
			'shift'   : '\u21d1',
			'meta1'   : '.?123',
			'accept'  : '\u21d3',
			'meta2'   : 'abc',
			'default' : '\u0430\u0431\u0432',
			'clear'   : 'Очистить',
			'enter'   : 'Найти',
		},

		layout: 'custom',
		//restrictInput : true, // Prevent keys not in the displayed keyboard from being typed in
		//maxLength : 6,
		//lockInput: true, // prevent manual keyboard entry
		useCombos : false, // don't want A+E to become a ligature
		preventPaste : true,  // prevent ctrl-v and right click
		autoAccept : true,
		usePreview: false, // no preveiw
		
		customLayout: {

			'default' : [
					"\u0439 \u0446 \u0443 \u043a \u0435 \u043d \u0433 \u0448 \u0449 \u0437 \u0445 \u044a {bksp}",
					"\u0444 \u044b \u0432 \u0430 \u043f \u0440 \u043e \u043b \u0434 \u0436 \u044d {enter}",
					"{shift} \u044f \u0447 \u0441 \u043c \u0438 \u0442 \u044c \u0431 \u044e {shift} {clear}",
					"{meta1} {meta2} {space} {meta1} {accept}"
				],

			'shift' : [
					"\u0419 \u0426 \u0423 \u041a \u0415 \u041d \u0413 \u0428 \u0429 \u0417 \u0425 \u042a {bksp}",
					"\u0424 \u042b \u0412 \u0410 \u041f \u0420 \u041e \u041b \u0414 \u0416 \u042d {enter}",
					"{shift} \u042f \u0427 \u0421 \u041c \u0418 \u0422 \u042c \u0411 \u042e {shift} {clear}",
					"{meta1} {meta2} {space} {meta1} {accept}"
				],
				
			'meta2': [
					'q w e r t y u i o p {bksp}',
					'a s d f g h j k l {enter}',
					'{shift} z x c v b n m {shift} {clear}',
					'{meta1} {default} {space} {meta1} {accept}'
				],
				
			'meta2-shift': [
					'Q W E R T Y U I O P {bksp}',
					'A S D F G H J K L {enter}',
					'{shift} Z X C V B N M {shift} {clear}',
					'{meta1} {default} {space} {meta1} {accept}'
				],
				
			'meta1': [
					'1 2 3 4 5 6 7 8 9 0 {bksp}',
					'- \u2116 : ; ( ) % = _ {enter}',
					'. , + ! " {sp:1} {clear}',
					'{default} {space} {meta2} {accept}'
				],
		},
		
		/*css : {
			buttonAction : 'ui-state-active'
		},*/

	});
});