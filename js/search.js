var gSearchInterface = {
        // Точка входа для поиска
        startSearch: function( ){
                var search_input = $('.search-input:last');
                var searchString = search_input.val();
                
                // Если в строке ничего не введено - открываем клавиатуру
                if ( searchString == '' ){
                        search_input.click();
                        search_input.focus();
                }
                // Если введено - переходим на страницу результатов поиска
                else{
                        // Спрятать клавиатуру
                        search_input.getkeyboard().close();   
                        
                        // Очищаем поле ввода
                        search_input.val('');
                        search_input.addClass('empty');
                        
                        // Закрываем текущий поиск если далее открывается новый
                        if ( search_input.data('close') == 'yes' ){
                                gChangePageEffect.close();
                        }
                        
                        // Открываем страницу результатов поиска
                        var link = '/server/screen10_search.php?search='
                                + searchString + search_input.data('where');
                        gChangePageEffect.openPage( link );
                }
        },
        
        
        // обработчики событий для поля ввода
        addHandlers: function(){
                var search_input = $('.search-input:last');
                
                // Подключение экранной клавиатуры для поля ввода
                gKeyboard.init( search_input );
                
                // По нажатию поля ввода, убираем дефолтный текст
                search_input.on( 'click', function(){
                        $(this).removeClass('empty');
                        $(this).select();
                });
                
                // Если полеьввода теряет фокус - ставим дефолтный текст
                search_input.on( 'blur', function(){
                        if ( $('.search-input:last').val() == '' ){
                                $(this).addClass('empty');
                        }
                });
                
                // Обработка кнопки Enter на клавиатуре
                $.keyboard.keyaction.enter = function(){
                        // Начать поиск
                        gSearchInterface.startSearch();
                };
        },
        
        
        // Обработчик кнопки Найти
        searchKeyHandler: function(){
                // Анимация нажатия кнопки
                gChangePageEffect.buttonPress( this );
                
                // Начать поиск
                gSearchInterface.startSearch();
        }
}
