/*
var gChangePageEffect = {
    openPage: function(link) {
        var self = gChangePageEffect;
        console.log("openPage call: ", link);

        if (link == '/server/template/main.php') {
            self.closeAll();
            console.log("fadePage -> goto main screen");
        }
        else {
            // Запрос на загрузку страницы
            $.ajax({
                url: link
            }).done(self.constructPage);
        }
    },

    // Возврат в основной экран
    closeAll: function() {
        // Удаляем все элементы screen2, кроме последней
        $('.screen2:last').removeClass('screen2').addClass('tmp');
        $('.screen2').remove();

        // А последнюю - анимируем, затем удаляем
        $('.tmp').removeClass('tmp').addClass('screen2').animate({
            'left': '1080px', 'opacity': '0'
        }, 300, "easeInCubic", function() {
            this.remove();
        });
    },

    // Закрыть текущую страницу
    close: function() {
        // анимируем, затем удаляем
        $('.screen2:last').animate({'opacity': '1'}, 300, "easeInCubic", function() {
            this.remove();
        });
    },

    // Добавляет новый экран поверх всех
    constructPage: function(data) {
        var self = gChangePageEffect;

        console.log('constructPage call');

        $('.page1').append('<div class="screen2"></div>');
        var new_page = $('.screen2:last');
        new_page.hide().html(data).css({
            'display': 'block', 'left': '1080px', 'opacity': '0'
        }).animate({
            'left': '0px', 'opacity': '0.4'
        }, 200, "easeInOutCubic").animate({'opacity': '1'}, 500);

        // Добавление обработчиков для новой страницы
        new_page.find(".btn").on('click', self.buttonClick_Handler);
        new_page.find(".animated-btn").on('click', function() {
            self.buttonPress(this);
        });
        new_page.find(".b-back-btn").on('click', self.backButtonClick_Handler);

        // Добавление полосы прокрутки к листам
        new_page.find('.b-list').jScrollPane();
        new_page.find('.b-search-results').jScrollPane();
        new_page.find('.scrollable').jScrollPane();

        // Прикручиваем клавиатуру в полю ввода
        gSearchInterface.addHandlers(new_page);

        // Обработчик кнопки "Показать на карте"
        new_page.find(".btn-show-map, .btn-show-path").on('click', function() {
            self.closeAll();
            gMapInterface.showMe2(); // переходим к текущей стеле
            gMapInterface.showFullPath($(this).data('id')); // строим маршрут
        });

        // Обработчик кнопки "Ближайший"
        new_page.find(".btn-show-nearest").on('click', function() {
            self.closeAll();
            gMapInterface.showMe2(); // переходим к текущей стеле
            var i = $(this).data('id');
            //alert(i);
            var splitted = i.split('|');
            //alert(splitted[0]);
            var id = Nearest(splitted);
            //alert(id);
            gMapInterface.showFullPath(id); // строим маршрут
        });

    },

    // начальные обработчики - для первого экрана
    addMainHandlers: function() {
        var self = gChangePageEffect;

        // Реализация перехода по ссылке с эффектами
        $(".btn").on('click', self.buttonClick_Handler);
        $(".b-icons .btn").on('click', self.closeAll);

        // Обработчики копки Разработчик
        $(".developer").on('click', function() {
            $('.b-developer-info').fadeIn();
        });
        $(".developer-close").on('click', function() {
            $('.b-developer-info').fadeOut();
        });

        // Нажатие на кнопку Я
        $(".b-iam-btn").on('click', function() {
            gMapInterface.showMe2();
        });

        // Обработка нажатия на кнопку Найти
        $(".b-search-btn").on('click', gSearchInterface.searchKeyHandler);

        // Обработчик кнопки "Показать на карте"
        $(".btn-show-map, .btn-show-path").on('click', function() {
            self.closeAll();
            var path_id = $(this).data('id');
            if (path_id == 'garderob') {
                path_id = getGarderobId();
                gMapInterface.showPath2(path_id); // Не открываем ничего
            }
            else {
                gMapInterface.showFullPath(path_id);
            }
        });

        // Инициализация поиска для главного экрана
        gSearchInterface.addHandlers();
    },

    // Обработчик нажатия кнопок
    buttonClick_Handler: function(ev) {
        ev.preventDefault();
        var self = gChangePageEffect;
        self.buttonPress(this);
        var link_load_page = $(this).attr("href");

        // Если кнопка ведет по ссылке - открываем страницу
        if (!link_load_page) {
            link_load_page = "/server/template/main.php";
        }
        self.openPage(link_load_page);
    },

    // Обработчик нажатия кнопки "Назад"
    backButtonClick_Handler: function() {
        var self = gChangePageEffect;
        self.buttonPress(this);
        $(this).parents('.screen2').animate({'left': '1080px', 'opacity': '0'}, 300, "easeInCubic", function() {
            this.remove();
        });
    },

    // Подкрашивает выбранную кнопку
    buttonPress: function(button) {
        var self = gChangePageEffect;
        self.buttonsReset();
        $(button).addClass("active");
        self.buttonPressed_timer = setTimeout(self.buttonsReset, 300);
    },

    // Убирает раскраску всех кнопок
    buttonsReset: function() {
        var self = gChangePageEffect;
        clearTimeout(self.buttonPressed_timer);
        $(".btn").removeClass("active");
        $(".animated-btn").removeClass("active");
        $(".b-back-btn").removeClass("active");
        $(".b-search-btn").removeClass("active");
    }
}

var __lastId;

var gMapInterface = {
    // Перейти на страницу с описанием магазина
    displayShopInfo: function(id) {
        var link = '/server/screen4_shop.php?shop=' + id;
        gChangePageEffect.openPage(link);
    },

    // Детская площадка
    displayKidsInfo: function(id) {
        var link = '/server/screen22_kids.php';
        gChangePageEffect.openPage(link);
    },

    // Комната матери и ребенка
    displayMotherRoomInfo: function(id) {
        var link = '/server/screen24_mother_room.php';
        gChangePageEffect.openPage(link);
    },

    // Третье место
    display3PlaceInfo: function(id) {
        var link = '/server/screen17_creative.php';
        gChangePageEffect.openPage(link);
    },

    // Банкоматы
    displayBankomatsInfo: function(id) {
        var link = '/server/screen18_bankomats.php';
        gChangePageEffect.openPage(link);
    },

    // Галерея
    displayGaleryInfo: function(id) {
        var link = '/server/screen21_gallery.php';
        gChangePageEffect.openPage(link);
    },

    // Стойка информации
    displayInfoInfo: function(id) {
        var link = '/server/screen23_info_stand.php';
        gChangePageEffect.openPage(link);
    },

    // Подсветить маркер текущей стелы
    showMe2: function() {
        showMe();
    },

    // Показать на карте путь до объекта
    showPath: function(id) {
        selectObject(id, true);
        this.showPath2(id);
    },

    showPath2: function(id) {
        cleanPath();
        drawPath(id);
        console.log("drawPath: ", id);
    },

    showFullPath: function(id) {
        console.log('showFullPath:', id);
        __lastId = id;
        this.showPath(id);
        showUIForPath();
    },

    showFullPathWithLastId: function() {
        console.log('showFullPath with lastId ', __lastId);
        this.showPath(__lastId);
        showUIForPath();
    },

    showFullPath2: function(id) {
        console.log('showFullPath2:' + id);
        __lastId = id;
        this.showPath2(id);
        showUIForPath();
    }
}

$(document).ready(function() {
    gChangePageEffect.addMainHandlers();
});
*/

(function($, window, document) {

    function showModal(modalType, modalHeader, modalContent, modalFooter) {

        $('.modal-backdrop').fadeIn(250);

        $('.modal-panel')
            .effect('drop', {
                'mode': 'hide',
                'direction': 'up',
                'easing': 'easeOutQuad'
            }, 100)
            .delay(300)
            .find('.company-title').html(modalHeader).end()
            .find('.company-description').html(modalContent).end()
            .find('.company-contacts').html(modalFooter).end()
            .effect('drop', {
                'mode': 'show',
                'direction': 'up',
                'easing': 'easeOutBack'
            }, 300);
    }

    function hideModal() {
        $('.modal-panel').effect('drop', {
            'mode': 'hide',
            'direction': 'up',
            'easing': 'easeOutQuad'
        }, 150);
        $('.modal-backdrop').fadeOut(500);
    }

    $(function() {
        $('.btn-floor').click(function() {
            if (!$(this).hasClass('active')) {
                console.log($(this).data('floor'));
                console.log('[data-floor=' + $(this).data('floor') + ']');
                $('.btn-floor.active').removeClass('active');
                $('.index-page')
                    .hide().addClass('hidden')
                    .filter('[data-floor=' + $(this).data('floor') + ']')
                    .show().removeClass('hidden');
                $(this).addClass('active');
            }
        });
        $('.index-link').click(function() {

            var modalHeader = $(this).data('company-name');
            var modalContent = 'Бережливое производство — это интерпретация ' +
                'идей Производственной системы компании Toyota американскими ' +
                'исследователями феномена Тойоты. Бережливое производство (lean ' +
                'production, lean manufacturing — англ. lean — «тощий, стройный, без жира»';
            var modalFooter = 'ООО ЛинСофт<br /> ' +
                '603152, Россия, Нижний Новгород,<br /> ' +
                'ул.Ларина, д .22, литер 1 Д, офис №4<br /> ' +
                'Телефон : +7(831) 4 - 144 - 844 Email: info@lean - soft.ru';
            showModal('popupCompany', modalHeader, modalContent, modalFooter);

        });
        $('.btn-modal-close').click(function() {
            hideModal();
        });

    });
}(window.jQuery, window, document));
